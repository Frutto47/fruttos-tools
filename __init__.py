import bpy
from bpy.props import PointerProperty
from bpy.types import Scene


bl_info = {
    'name': "Frutto's tools",
    'blender': (4, 2, 3),
    'category': 'Object',
    "author": "Frutto",
    'version': (0, 1),
    "location": "3D View > Properties > FT",
    "description": "Frutto's Tools for faster working",
    "doc_url": "https://gitlab.com/Frutto47/fruttos-tools",
    'support': 'COMMUNITY',
    'warning': '',
    'show_expanded': False,
    '_init': None,
    'tracker_url': '',
}


def register():
    # classses registration
    # properties
    from .properties import ft_properties
    ft_properties.class_register()


    # operators
    from .operators import characters_creation_ops
    characters_creation_ops.class_register()


    from .operators import fuse_skeletons_ops
    fuse_skeletons_ops.class_register()


    from .operators import ninja_ripper_ops
    ninja_ripper_ops.class_register()


    from .operators import object_translation_ops
    object_translation_ops.class_register()


    from .operators import optimization_ops
    optimization_ops.class_register()


    from .operators import outfit_ops
    outfit_ops.class_register()


    from .operators import time_ops
    time_ops.class_register()


    from .operators import vertex_group_ops
    vertex_group_ops.class_register()


    from .operators import node_tools_ops
    node_tools_ops.class_register()


    #panels
    from .ui import character_creation_pnl
    character_creation_pnl.class_register()


    from .ui import fuse_skeletons_pnl
    fuse_skeletons_pnl.class_register()


    from .ui import mesh_optimization_pnl
    mesh_optimization_pnl.class_register()


    from .ui import ninja_ripper_pnl
    ninja_ripper_pnl.class_register()


    from .ui import object_translation_pnl
    object_translation_pnl.class_register()


    from .ui import outfits_pnl
    outfits_pnl.class_register()


    from .ui import vertex_groups_pnl
    vertex_groups_pnl.class_register()

    from .ui import node_tools_pnl
    node_tools_pnl.class_register()


    Scene.ft_props = PointerProperty(type=ft_properties.FT_scene_props)


def unregister():
    # classses unregistrationolll
    # operators
    from .operators import characters_creation_ops
    characters_creation_ops.class_unregister()


    from .operators import fuse_skeletons_ops
    fuse_skeletons_ops.class_unregister()


    # from .operators import ninja_ripper_ops
    # ninja_ripper_ops.class_unregister()


    from .operators import object_translation_ops
    object_translation_ops.class_unregister()


    from .operators import optimization_ops
    optimization_ops.class_unregister()


    from .operators import outfit_ops
    outfit_ops.class_unregister()


    from .operators import time_ops
    time_ops.class_unregister()


    from .operators import vertex_group_ops
    vertex_group_ops.class_unregister()



    #panels
    from .ui import character_creation_pnl
    character_creation_pnl.class_unregister()


    from .ui import fuse_skeletons_pnl
    fuse_skeletons_pnl.class_unregister()


    from .ui import mesh_optimization_pnl
    mesh_optimization_pnl.class_unregister()


    # from .ui import ninja_ripper_pnl
    # ninja_ripper_pnl.class_unregister()


    from .ui import object_translation_pnl
    object_translation_pnl.class_unregister()


    from .ui import outfits_pnl
    outfits_pnl.class_unregister()


    from .ui import vertex_groups_pnl
    vertex_groups_pnl.class_unregister()

    from .ui import node_tools_pnl
    node_tools_pnl.class_unregister()



    # properties
    from .properties import ft_properties
    ft_properties.class_unregister()

    del Scene.ft_props
