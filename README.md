# Frutto's tools

![](https://img.shields.io/badge/Blender%20version-4.3.9-F5792A?style=for-the-badge&logo=blender)
![](https://img.shields.io/badge/Python%20version-3.11.9-FFE872?style=for-the-badge&logo=python)
![](https://img.shields.io/badge/FFUS%20version-1.3.2-9BE737?style=for-the-badge)

## What is frutto's tools?
#### Frutto's tools is an addon for blender, that combines many useful features, that speeds up routine work in blender.


## Installation
Install easy as a simple blender add-on.

1. Code -> download source code (.zip)
2. Blender -> Edit -> Preferences -> Addons -> Install from disk (in drop down menu at top right arrow)
3. Select .zip file -> Install From Disk.
4. Tick a checkbox next to the addon to activate it.
5. FT tab should now appear in N panel of 3D Viewport.

## Notes
1. This addon is written for blender version 4.3.0 and above
