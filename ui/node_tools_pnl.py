import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_node_tools(FT_common_panel, Panel):
    bl_label = 'Node tools'
    bl_options = set()


    def draw(self, context):
        layout = self.layout
        props = context.scene.ft_props

        box = layout.box()
        box.prop(props, 'node_location_x')
        box.prop(props, 'node_location_y')
        box.operator('ft.get_node_location')
        box.operator('ft.set_node_location')
        try:
            node_location = context.active_object.active_material.node_tree.nodes.active.location
        except AttributeError:
            node_location = (None, None)
        box.label(text=f'Location X: {node_location[0]}')
        box.label(text=f'location Y: {node_location[1]}')

        
        

classes = [
    FT_PT_node_tools,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
