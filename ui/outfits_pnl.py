import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_outfits(FT_common_panel, Panel):
    bl_label = 'Outfits'
    bl_options = set()


    def draw(self, context):
        layout = self.layout
        props = context.scene.ft_props

        row = layout.row()
        row.operator('ft.make_outfit_collections')

        row = layout.row()
        row.prop(props, 'location_offset')

        row = layout.row()
        row.operator('ft.offset_collections')


classes = [
    FT_PT_outfits,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
