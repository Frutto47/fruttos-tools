import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_mesh_translation(FT_common_panel, Panel):
    bl_label = 'Object translation'


    def draw(self, context):
        props = context.scene.ft_props
        layout = self.layout

        row = layout.row()
        row.label(text='Change LocRotScale of selected objects to active object')

        if context.active_object:
            row = layout.row()
            row.label(text=f'Active object: {context.active_object.name}')

        row = layout.row()
        obj_count = 0 if len(context.selected_objects) == 0 else len(context.selected_objects) - 1
        row.label(text=f'Objects to translate: {obj_count}')

        row = layout.row()
        row.prop(props, 'apply_loc_rot_scale')

        row = layout.row()
        row.operator('ft.loc_rot_scale_to_active_object')


classes = [
    FT_PT_mesh_translation,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
