import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_vertex_groups_tools(FT_common_panel, Panel):
    bl_label = 'Vertex Groups tools'

    def draw(self, context):
        layout = self.layout
        props = context.scene.ft_props

        row = layout.row()
        row.operator('ft.remove_unused_vertex_groups')


classes = [
    FT_PT_vertex_groups_tools,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
