import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_character_creation(FT_common_panel, Panel):
    bl_label = "Character creation"


    def draw(self, context):
        props = context.scene.ft_props
        layout = self.layout

        row = layout.row()
        row.label(text="Character name:")

        row = layout.row()
        row.prop(props, "character_name", text='')

        row = layout.row()
        row.operator('ft.prefix_materials_names')

        row = layout.row()
        row.operator('ft.clear_prefix_materials_names')

        layout.separator()

        row = layout.row()
        row.label(text='Path to folder with genesis textures:')

        row = layout.row()
        row.prop(props, 'path_to_genesis_textures', text='')

        row = layout.row()
        row.label(text='Path to folder with character textures:')

        row = layout.row()
        row.prop(props, 'path_to_character_textures', text='')


        row = layout.row()
        row.label(text='Change texture resolution in:')

        
        row = layout.row()
        row.prop(props, "selected_materials", expand=True)

        row = layout.row()
        row.prop(props, "desired_resolution", expand=True)

        row = layout.row()
        row.operator('ft.change_texure_resolution')


classes = [
    FT_PT_character_creation,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
