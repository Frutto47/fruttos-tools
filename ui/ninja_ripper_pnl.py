import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_ninja_ripper(FT_common_panel, Panel):
    bl_label = 'Ninja ripper output fix'


    def draw(self, context):
        layout = self.layout
        props = context.scene.ft_props

        row = layout.row()
        row.label(text='Only in edit mode:')
        row.operator('ft.fix_rotation')

        row = layout.row()
        row.operator('ft.fix_x_axis_mirror')

        row = layout.row()
        row.operator('ft.use_as_ground')

        row = layout.row()
        row.operator('ft.fix_scale')

        row = layout.row()
        if props.selection_only_for_mesh_removal:
            objects = [ob for ob in context.selected_objects if ob.type == "MESH"]
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == "MESH"]
        text = f'Only selected objects ({len(objects)})'
        row.prop(props, 'selection_only_for_mesh_removal', text=text)

        row = layout.row()
        row.prop(props, 'remove_mesh_with_no_material')

        row = layout.row()
        row.prop(props, 'remove_mesh_with_no_uv')

        row = layout.row()
        row.operator('ft.remove_meshes_without_something')

        row = layout.row()

        row.prop(props, 'desired_uv_map')

        row = layout.row()
        if props.selection_only_for_uv_set:
            objects = [ob for ob in context.selected_objects if ob.type == "MESH"]
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == "MESH"]
        text = f'Only selected objects ({len(objects)})'
        row.prop(props, 'selection_only_for_uv_set', text=text)

        row.operator('ft.set_desired_uv')


classes = [
    FT_PT_ninja_ripper,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
