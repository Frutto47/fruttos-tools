import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_fuse_skeletons(FT_common_panel, Panel):
    bl_label = "Fuse Skeletons"


    def draw(self, context):
        props = context.scene.ft_props
        layout = self.layout
        layout.label(text="Select the Skeletons you want to fuse > press 'Fuse Skeletons'")
        layout.label(text="This will delete all bones with the same names")
        layout.label(text="and the children will be parented to the base skeleton")
        layout.separator()
        layout.label(text="Select Skeleton to Fuse to:")
        # layout.operator_menu_enum("fuseskeletons.write_select_object", "select_objects")
        layout.prop(props, "armature_objects_in_scene")
        layout.prop(props, "material_objects_in_scene")

        layout.operator("fuseskeletons.fuse_selected")


classes = [
    FT_PT_fuse_skeletons,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
