import bpy
from bpy.types import Panel
from .common_panel import FT_common_panel


class FT_PT_mesh_optimisation(FT_common_panel, Panel):
    bl_label = 'Mesh optimisation'

    def draw(self, context):
        layout = self.layout
        props = context.scene.ft_props

        box = layout.box()
        box.prop(props, 'remove_doubles_enabled')

        if props.remove_doubles_enabled:
            box.prop(props, 'merge_distance')

        box = layout.box()
        box.prop(props, 'to_quads_enabled')

        if props.to_quads_enabled:
            box.prop(props, 'max_face_angle')
            box.prop(props, 'max_shape_angle')

        box = layout.box()
        box.prop(props, 'shade_smooth_enabled')

        if props.shade_smooth_enabled:
            box.prop(props, 'smooth_angle')

        row = layout.row()
        row.scale_y = 1.3
        row.operator('ft.reset_opt_params')

        row = layout.row()
        row.prop(props, 'selection_only_for_optimisation')

        if props.selection_only_for_optimisation:
            objects = [ob for ob in context.selected_objects if ob.type == "MESH"]
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == "MESH"]
        if not len(objects):
            text = 'No objects'
        else:
            text = f'Optimise ({len(objects)}) objects'
        
        row = layout.row()
        row.scale_y = 1.5
        row.operator('ft.optimise_objects', text=text, icon='SELECT_EXTEND')


classes = [
    FT_PT_mesh_optimisation,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
