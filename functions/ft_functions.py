import bpy
from bpy.types import Material

from pathlib import Path


def get_armature_objects(self, context) -> list:
    items = []
    # iterate over all objects
    # in scene and add an index to them
    for scene_object_index, scene_object in enumerate(ob for ob in bpy.data.objects if ob.type == "ARMATURE"):
        items.append((str(scene_object_index), scene_object.name, scene_object.name))
    return items
    

def get_material_objects(self, context) -> list:
    items = []
    # iterate over all objects
    # in scene and add an index to them
    for material_index, material in enumerate(ob for ob in bpy.data.materials):
        if 'dots stroke' in material.name.lower():
            continue
        items.append((str(material_index), material.name, material.name))
    return items


def is_material_present(context) -> bool:
    for mat_slot in context.active_object.material_slots:
        # skip empty material slot
        if not mat_slot.name:
            continue
        return True
    return False


def get_materials_to_change(context, selected_variant: str) -> set[Material]:
    materials_to_change_textures = set()
    if selected_variant == 'ALL':
        for mat_slot in context.active_object.material_slots:
        # skip empty material slot
            if not mat_slot.name:
                continue
            materials_to_change_textures.add(mat_slot.material)
    elif selected_variant == 'SELECTED':
        materials_to_change_textures.add(context.active_object.active_material)

    return materials_to_change_textures


def get_images_from_materials_nodes(materials_to_change_textures: set[Material], desired_resolution: tuple[int, int]):
    images = set()
    for mat in materials_to_change_textures:
    # get all image texture nodes, in material node tree
        
        for node in [node for node in mat.node_tree.nodes if node.bl_idname == 'ShaderNodeTexImage']:
            image = node.image
            # skip nodes without image texture and without filepath
            if not (image and image.source == 'FILE' and image.filepath):
                continue
            # if image resolution is equal to desired resolution, continue
            if (image.size[0], image.size[1]) == desired_resolution:
                continue
            images.add(image)
    return images


def get_all_filepaths_in_desired_paths(desired_paths: list[Path]) -> dict:
    image_stem_path_dict = dict()
    for desired_path in desired_paths:
        for filepath in desired_path.glob('**/*'):
            if not filepath.is_file():
                continue
            image_stem_path_dict[filepath.stem.lower()] = filepath
    return image_stem_path_dict
