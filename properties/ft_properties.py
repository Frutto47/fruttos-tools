from bpy.types import PropertyGroup
from bpy.props import PointerProperty, FloatProperty, IntProperty, StringProperty, BoolProperty, EnumProperty
from math import pi
from ..functions.ft_functions import *



class FT_scene_props(PropertyGroup):
    remove_doubles_enabled: BoolProperty(
        name='Remove double vertices',
        description='Remove double vertices (Remove vertices very close coordinates)',
        default=True,
    )
    merge_distance: FloatProperty(
        name='Merge distance',
        default=0.0001,
        min=0,
        max=1,
    )
    to_quads_enabled: BoolProperty(
        name='Triangles to quads',
        description='Change triangles to quads',
        default=True,
    )
    max_face_angle: FloatProperty(
        name='Max face angle',
        default=40 * (pi / 180),
        min=0,
        max=180 * (pi / 180),
        subtype='ANGLE'
    )
    max_shape_angle: FloatProperty(
        name='Max shape angle',
        default=40 * (pi / 180),
        min=0,
        max=180 * (pi / 180),
        subtype='ANGLE'
    )
    shade_smooth_enabled: BoolProperty(
        name='Shade smooth',
        description='Make shade smooth',
        default=True,
    )

    smooth_angle: FloatProperty(
        name='Smooth angle',
        default=50 * (pi / 180),
        min=0,
        max=180 * (pi / 180),
        subtype='ANGLE'
    )

    apply_loc_rot_scale: BoolProperty(
        name='Apply LocRocScale after translation',
        description='Apply LocRocScale after translation',
        default=True,
    )
    time: StringProperty(
        name='Time',
        description='Time for FT',
        default=''
    )
    selection_only_for_optimisation: BoolProperty(
        name='Only selected objects',
        description='Check if you want to affect only selected objects',
        default=True,
    )
    selection_only_for_uv_set: BoolProperty(
        name='Only selected objects',
        description='Check if you want to affect only selected objects',
        default=True,
    )
    selection_only_for_mesh_removal: BoolProperty(
        name='Only selected objects',
        description='Check if you want to affect only selected objects',
        default=True,
    )
    remove_mesh_with_no_material: BoolProperty(
        name='No materials',
        description='Check if you want to remove objects with no materials',
        default=True,
    )
    remove_mesh_with_no_uv: BoolProperty(
        name='No UV',
        description='Check if you want to remove objects with no YVs',
        default=True,
    )
    desired_uv_map: StringProperty(
        name='Desired UV',
        description='Enter desired UV',
        default=''
    )
    armature_objects_in_scene: EnumProperty(
        items=get_armature_objects,
        name='All armature objects',
        description='All armature objects in scene',
    )
    material_objects_in_scene: EnumProperty(
        items=get_material_objects,
        name='All material objects',
        description='All material objects in scene',
    )
    location_offset: IntProperty(
        name='Location x offset',
        default=1,
        min=0,
        max=100
    )

    # model creation props
    character_name: StringProperty(
        name='character name',
        description='Name of the character you are editing',
        default='Meg Thomas'
    )

    selected_materials: EnumProperty(
        name='Selected materials',
        description='Choose materials you want to operate on',
        items=[('ALL', 'All materials', 'All materials'),
                ('SELECTED', 'Selected materials', 'Selected materials')
            ]
    )
    desired_resolution: EnumProperty(
        name='Material texture size',
        description='Choose preferred texture resolution',
        items=[ ('1024', '1K', 'Change textures resolution to 1024x1024'),
                ('2048', '2K', 'Change textures resolution to 2048x2048'),
                ('4096', '4K', 'Change textures resolution to 4096x4096'),
            ]
    )
    path_to_genesis_textures: StringProperty(
        name='Path to folder with genesis textures',
        description='Path to folder with genesis textures, that needs to be changed',
        default=r'G:\DBD research\01 DBD Nude Models Creation\00 Genesis 9 base\textures',
        subtype='DIR_PATH',
    )
    path_to_character_textures: StringProperty(
        name='Path to folder with character textures',
        description='Path to folder with character textures, that needs to be changed',
        default=r'G:\DBD research\01 DBD Nude Models Creation\Survivors\Girls\01 Meg Thomas\03 Final\textures',
        subtype='DIR_PATH',
    )
    
    # node tools properties
    node_location_x: IntProperty(
    name='X coordinate for node',
    default=0,
    )
    node_location_y: IntProperty(
    name='Y coordinate for node',
    default=0
    )
classes = [
    FT_scene_props,
]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
