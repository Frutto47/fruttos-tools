import bpy
from bpy.types import Operator


import re



class FT_OT_make_outfit_collections(Operator):
    bl_idname = "ft.make_outfit_collections"
    bl_label = "Make outfit collections"
    bl_description = "Make outfit collections based on active collection"
    bl_options = {'REGISTER'}


    def execute(self, context):
        # delete all collections
        for coll in bpy.data.collections:
            # unlink all objects in collection before removing it
            for obj in coll.objects:
                coll.objects.unlink(obj)

            # remove collection
            bpy.data.collections.remove(coll)

        # create meshes outfits dict:
        # example:
        # meshed_outfits = {'01': [data.objects['legs01'], ]}
        meshes_outfits = {}
        for obj in bpy.data.objects:
            number_is_found = re.search(r"\d+", obj.name.lower())
            if number_is_found:
                obj_outfit_num = number_is_found.group()

            meshes_outfits[obj_outfit_num] = meshes_outfits.get(obj_outfit_num, []) + [obj]
        
        # sort meshes_outfits dict
        meshes_outfits = {key: value for key, value in sorted(meshes_outfits.items(), key = lambda item: (len(item[0]), item[0]))}

        # create collections with outfit number names
        for num, objects in meshes_outfits.items():
            # create collection with outfit name
            coll = bpy.data.collections.new(f'Outfit {num}')
            # link collection to scene master collection
            context.scene.collection.children.link(coll)

            # hide objects inside collectioon for optimisation
            coll.hide_viewport = True

            # link all objects to collection
            for obj in objects:
                # check if object is in master collection, if yes, then unlink it
                if obj in list(context.scene.collection.objects):
                   context.scene.collection.objects.unlink(obj)

                # link object to corresponding outfit collection 
                coll.objects.link(obj)
        

        self.report({'INFO'}, f'Linked {len([item for outfit_name in meshes_outfits for item in  meshes_outfits[outfit_name]])} object(s) in {len(meshes_outfits)} collection(s)')
        return {'FINISHED'}


class FT_OT_offset_collections(Operator):
    bl_idname = "ft.offset_collections"
    bl_label = "Location offset collections"
    bl_description = "Add location offset to allcollections"
    bl_options = {'REGISTER'}


    def execute(self, context):
        props = context.scene.ft_props

        # reset location of all objects
        for coll in bpy.data.collections:
            for obj in coll.objects:
                obj.location.x = 0


        location = 0
        offset = props.location_offset
        for coll in bpy.data.collections:
            for obj in coll.objects:
                obj.location.x = location
            # unhide objects inside collection
            coll.hide_viewport = False
            location += offset

        return {'FINISHED'}


classes = [
    FT_OT_make_outfit_collections,
    FT_OT_offset_collections
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
