import bpy
from bpy.types import Operator



class FT_OT_set_node_location(Operator):
    bl_idname = "ft.set_node_location"
    bl_label = "Set node location"
    bl_description = "Set node location to desired coords"
    bl_options = {'REGISTER', 'UNDO'}


    @classmethod
    def poll(cls, context):
        if not context:
            return False

        if not context.active_object:
            return False
        if not context.active_object.active_material:
            return False
        return True


    def execute(self, context):
        props = context.scene.ft_props
        node_location = context.active_object.active_material.node_tree.nodes.active.location
        
        node_location.x = props.node_location_x
        node_location.y = props.node_location_y

        return {'FINISHED'}


class FT_OT_get_node_location(Operator):
    bl_idname = "ft.get_node_location"
    bl_label = "Get node location"
    bl_description = "Get location of active node"
    bl_options = {'REGISTER', 'UNDO'}


    @classmethod
    def poll(cls, context):
        if not context:
            return False

        if not context.active_object:
            return False
        if not context.active_object.active_material:
            return False
        return True


    def execute(self, context):
        props = context.scene.ft_props
        loc = context.active_object.active_material.node_tree.nodes.active.location
        props.node_location_x, props.node_location_y = int(loc.x), int(loc.y)
        
        return {'FINISHED'}


classes = [
    FT_OT_get_node_location,
    FT_OT_set_node_location,
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
