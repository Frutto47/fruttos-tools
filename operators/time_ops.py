import bpy
from bpy.types import Operator


class FT_OT_time_update(Operator):
    bl_idname = "ft.time_update"
    bl_label = "Update time"

    def execute(self, context):
        props = context.scene.ft_props
        current_time = datetime.datetime.now()
        props.time = current_time.strftime("%Y %b %d, %H:%M:%S")

        return {'FINISHED'}
    bl_idname = "ft.loc_rot_scale_to_active_object"
    bl_label = "Translate to active object"

    def execute(self, context):
        props = context.scene.ft_props
        active_obj = context.active_object
        selected_objects = context.selected_objects
        selected_objects_without_active = set(selected_objects).difference({active_obj})
        print(f'source_obj = {selected_objects_without_active}')
        print(f'active_obj = {active_obj}')

        ## debug, doesn't work
        for ob in selected_objects_without_active:
            ob.location = active_obj.location
            ob.rotation_euler = active_obj.rotation_euler
            ob.scale = active_obj.scale
            if props.apply_loc_rot_scale:
                bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

        self.report({'INFO'}, 'Loc Rot Scale transferred to active object')
        return {'FINISHED'}


classes = [
    FT_OT_time_update
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
