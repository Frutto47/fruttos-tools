import bpy
from bpy.types import Operator, Material

from .. functions.ft_functions import *

import time
from pathlib import Path


class FT_OT_prefix_materials_names(Operator):
    bl_idname = "ft.prefix_materials_names"
    bl_label = "Prefix material names"
    bl_description = "Add a prefix to material names"
    bl_options = {'REGISTER'}


    @classmethod
    def poll(cls, context):
        # context must be in the scene
        if not context:
            return False

        # active object must be present
        if not context.active_object:
            return False

        # active object type must be mesh
        if context.active_object.type != 'MESH':
            return False

        # mesh object must at least have one material
        if not is_material_present(context):
            return False
        return True


    def execute(self, context):
        props = context.scene.ft_props

        # add character_name prefix to a material name,
        # but only if there is no any substrings, containig character_name
        for mat_slot in context.active_object.material_slots:
        # skip empty material slot
            if not mat_slot.name:
                continue

            mat_name = mat_slot.material.name
            if props.character_name.lower() in mat_name.lower():
                continue
            mat_slot.material.name = f'{props.character_name} - {mat_name}'

        return {'FINISHED'}


class FT_OT_clear_prefix_materials_names(Operator):
    bl_idname = "ft.clear_prefix_materials_names"
    bl_label = "Creat prefix material names"
    bl_description = "Delete a prefix from material names"
    bl_options = {'REGISTER'}


    @classmethod
    def poll(cls, context):
        # context must be in the scene
        if not context:
            return False

        # active object must be present
        if not context.active_object:
            return False

        # active object type must be mesh
        if context.active_object.type != 'MESH':
            return False

        # mesh object must at least have one material
        if not is_material_present(context):
            return False
        return True


    def execute(self, context):
        props = context.scene.ft_props

        # add character_name prefix to a material name,
        # but only if there is no any substrings, containig character_name
        for mat_slot in context.active_object.material_slots:
        # skip empty material slot
            if not mat_slot.name:
                continue

            mat_name = mat_slot.material.name
            if len(mat_name.split(' - ')) >= 2:
                mat_slot.material.name = f'{mat_name.split(" - ")[-1]}'
        return {'FINISHED'}


class FT_OT_change_texture_resolution(Operator):
    bl_idname = "ft.change_texure_resolution"
    bl_label = "Change texture resolution"
    bl_description = "Change textures resolution to a desired variant"
    bl_options = {'REGISTER'}


    @classmethod
    def poll(cls, context):
        # context must be in the scene
        if not context:
            return False

        # active object must be present
        if not context.active_object:
            return False

        # active object type must be mesh
        if context.active_object.type != 'MESH':
            return False

        # mesh object must at least have one material
        if not is_material_present(context):
            return False
        return True


    def execute(self, context):
        props = context.scene.ft_props

        materials_to_change_textures : set[Material] = get_materials_to_change(context, props.selected_materials)
        desired_resolution = (int(props.desired_resolution), int(props.desired_resolution))
        desired_folder = props.desired_resolution[0] + 'K'
        desired_paths = [Path(props.path_to_genesis_textures) / desired_folder,
                        Path(props.path_to_character_textures) / desired_folder
            ]

        images_to_change_resolution : set[Images] = get_images_from_materials_nodes(materials_to_change_textures, desired_resolution)
        if not images_to_change_resolution:
            self.report({'WARNING'}, 'No images to change')
            return {'FINISHED'}
        
        # get all available image filepaths in genesis and character folders
        # this keys are image stem values and keys - are full paths to these textures
        all_images_filepaths_in_desired_folder: dict = get_all_filepaths_in_desired_paths(desired_paths)


        # for each image check, if there is an image file with the same name, but in desired texture folder,
        # if yes, then change it to corresponging filepath
        filepaths_changed = 0
        for image in images_to_change_resolution:
            image_path = Path(image.filepath)
            if all_images_filepaths_in_desired_folder.get(image_path.stem.lower()):

                # check if existing image filepath mathes desired image filepath, if yes, skip
                if str(image_path).lower() == str(all_images_filepaths_in_desired_folder[image_path.stem.lower()]).lower():
                    continue

                print(f'Changing from {str(image_path)} to {str(all_images_filepaths_in_desired_folder[image_path.stem.lower()])}')
                image.filepath = str(all_images_filepaths_in_desired_folder[image_path.stem.lower()])
                filepaths_changed += 1

        self.report({'INFO'}, f'Changed resolution of {filepaths_changed}/{len(images_to_change_resolution)} textures')
        return {'FINISHED'}



classes = [
    FT_OT_prefix_materials_names,
    FT_OT_clear_prefix_materials_names,
    FT_OT_change_texture_resolution
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
