import bpy
from bpy.types import Operator



class FT_OT_remove_unused_vertex_groups(Operator):
    bl_idname = "ft.remove_unused_vertex_groups"
    bl_label = "Remove unused vertex groups"
    bl_description = "Remove all vertex groups of active object, that have no any weights assigned to it"
    bl_options = {'REGISTER'}


    @classmethod
    def poll(cls, context):
        # context must be in the scene
        if not context:
            return False

        if context.active_object:
            # active object must be mesh
            if context.active_object.type != 'MESH':
                return False
            return True
        return False


    def execute(self, context):
        obj = context.object
        used_groups_indices = set()
        # Go through each vertex and add the group indices to the set
        for vertex in obj.data.vertices:
            for group in vertex.groups:
                used_groups_indices.add(group.group)

        used_groups_names = set([gr.name for gr in obj.vertex_groups if gr.index in used_groups_indices])

        count = 0
        for group in obj.vertex_groups:
            if group.name not in used_groups_names:
                obj.vertex_groups.remove(group)
                count += 1
        self.report({'INFO'}, f'Removed {count} vertex groups')
        return {'FINISHED'}


classes = [
    FT_OT_remove_unused_vertex_groups
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
