import bpy
from bpy.types import Operator

from math import pi


class FT_OT_optimise_objects(Operator):
    bl_idname = "ft.optimise_objects"
    bl_label = "Optimise objects"
    bl_description = "Optimise objects with desired parameters"
    bl_options = {"REGISTER", "UNDO"}


    @classmethod
    def poll(self, context):
        if context.active_object.mode != 'OBJECT':
            return False
        
        props = context.scene.ft_props
        if props.selection_only_for_optimisation:
            objects = [ob for ob in context.selected_objects if ob.type == 'MESH']
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == 'MESH']

        if not objects:
            return False
        return True


    def execute(self, context):
        props = context.scene.ft_props
        if props.selection_only_for_optimisation:
            objects = [ob for ob in context.selected_objects if ob.type == 'MESH']
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == 'MESH']

        active_object = context.active_object
        selected_objects = context.selected_objects

        # active object MUST be 'MESH' type, otherwise an exception occurs (context is incorrect)
        # ask a question to somebody, why this happens???
        context.view_layer.objects.active = objects[0]

        # think how to optimise this
        bpy.ops.object.select_all(action='DESELECT')
        for ob_count, ob in enumerate(objects, start=1):
            ob.select_set(True)
            if props.remove_doubles_enabled:
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='SELECT')
                bpy.ops.mesh.remove_doubles(threshold=props.merge_distance)
                bpy.ops.mesh.select_all(action='DESELECT')
                bpy.ops.object.editmode_toggle()

            if props.to_quads_enabled:
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='SELECT')
                bpy.ops.mesh.tris_convert_to_quads( face_threshold=props.max_face_angle,
                                                    shape_threshold=props.max_shape_angle,
                                                    uvs=True)

                bpy.ops.mesh.select_all(action='DESELECT')
                bpy.ops.object.editmode_toggle()

            if props.shade_smooth_enabled:
                bpy.ops.object.shade_auto_smooth(angle=props.smooth_angle)
            ob.select_set(False)

        # selecting objects back, that were selected before operator call
        for ob in selected_objects:
            ob.select_set(True)

        # returning active object back
        context.view_layer.objects.active = active_object

        if ob_count == 1:
            self.report({'INFO'}, f'{ob_count} mesh object successfully optimised')
        else:
            self.report({'INFO'}, f'{ob_count} mesh objects successfully optimised')

        return {'FINISHED'}


class FT_OT_reset_opt_params(Operator):
    bl_idname = "ft.reset_opt_params"
    bl_label = "Reset optimisation parameters"

    def execute(self, context):
        props = context.scene.ft_props
        props.remove_doubles_enabled = True
        props.merge_distance = 0.0001
        props.to_quads_enabled = True
        props.max_face_angle = 40 * (pi / 180)
        props.max_shape_angle = 40 * (pi / 180)
        props.shade_smooth_enabled = True
        props.smooth_angle = 50 * (pi / 180)
        self.report({'INFO'}, 'Optimisation parameters reset')
        return {'FINISHED'}

classes = [
    FT_OT_optimise_objects,
    FT_OT_reset_opt_params
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
