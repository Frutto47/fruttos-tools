import bpy
from bpy.types import Operator


class FT_OT_remove_mesh_without_something(Operator):
    bl_idname = "ft.remove_meshes_without_something"
    bl_label = "Remove mesh"


    def execute(self, context):
        props = context.scene.ft_props
        if props.selection_only_for_mesh_removal:
            objects = [ob for ob in context.selected_objects if ob.type == 'MESH']
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == 'MESH']

        bpy.ops.object.select_all(action='DESELECT')

        for ob in objects:
            if props.remove_mesh_with_no_material and not list(ob.material_slots):
                ob.select_set(True)
                bpy.ops.object.delete(use_global=True)
                continue
            if props.remove_mesh_with_no_uv and not list(ob.data.uv_layers):
                ob.select_set(True)
                bpy.ops.object.delete(use_global=True)

        return {'FINISHED'}
    bl_idname = "ft.loc_rot_scale_to_active_object"
    bl_label = "Translate to active object"

    def execute(self, context):
        props = context.scene.ft_props
        active_obj = context.active_object
        selected_objects = context.selected_objects
        selected_objects_without_active = set(selected_objects).difference({active_obj})
        print(f'source_obj = {selected_objects_without_active}')
        print(f'active_obj = {active_obj}')

        ## debug, doesn't work
        for ob in selected_objects_without_active:
            ob.location = active_obj.location
            ob.rotation_euler = active_obj.rotation_euler
            ob.scale = active_obj.scale
            if props.apply_loc_rot_scale:
                bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)

        self.report({'INFO'}, 'Loc Rot Scale transferred to active object')
        return {'FINISHED'}


class FT_OT_fix_rotation(Operator):
    bl_idname = "ft.fix_rotation"
    bl_label = "Fix rotation"

    def execute(self, context):
        bpy.ops.view3d.view_axis(type='TOP', align_active=True)
        bpy.ops.object.editmode_toggle()
        bpy.ops.object.empty_add(type='PLAIN_AXES', align='VIEW')
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)

        bpy.ops.object.rotation_clear(clear_delta=False)
        bpy.ops.object.location_clear(clear_delta=False)
        bpy.ops.object.scale_clear(clear_delta=False)
        context.object.rotation_euler[0] = -1.5708

        return {'FINISHED'}


class FT_OT_fix_scale(Operator):
    bl_idname = "ft.fix_scale"
    bl_label = "Fix scale"

    def execute(self, context):
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.transform.resize(value=(0.01, 0.01, 0.01))
        bpy.ops.view3d.view_all(center=False)
        return {'FINISHED'}


class FT_OT_fix_x_axis_mirror(Operator):
    bl_idname = "ft.fix_x_axis_mirror"
    bl_label = "Fix X-axis mirror"

    def execute(self, context):
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.transform.resize(value=(-1, 1, 1), orient_type='GLOBAL',
                                 orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL',
                                 constraint_axis=(True, False, False), mirror=False, use_proportional_edit=False,
                                 proportional_edit_falloff='SMOOTH', proportional_size=1,
                                 use_proportional_connected=False, use_proportional_projected=False, snap=False,
                                 snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST',
                                 use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True,
                                 use_snap_selectable=False)
        bpy.ops.view3d.view_all(center=False)
        return {'FINISHED'}


class FT_OT_use_as_ground(Operator):
    bl_idname = "ft.use_as_ground"
    bl_label = "Use as ground"


    def execute(self, context):
        object = context.active_object
        # enter edit mode
        bpy.ops.object.editmode_toggle()

        # select all verts
        bpy.ops.mesh.select_all(action='SELECT')

        # snap 3D cursor to selected
        bpy.ops.view3d.snap_cursor_to_selected()

        # ezit edit mode
        bpy.ops.object.editmode_toggle()

        # set origin to 3D cursor
        bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')

        # save inverse location
        location = object.location * -1

        # clear location
        bpy.ops.object.location_clear(clear_delta=False)

        # select all other objects
        bpy.ops.object.select_all(action='INVERT')
        bpy.ops.transform.translate(value=location)

        return {'FINISHED'}


class FT_OT_set_desired_uv(Operator):
    bl_idname = "ft.set_desired_uv"
    bl_label = "Set desired UV"

    def execute(self, context):
        props = context.scene.ft_props

        if props.selection_only_for_uv_set:
            objects = [ob for ob in context.selected_objects if ob.type == 'MESH']
        else:
            objects = [ob for ob in bpy.data.objects if ob.type == 'MESH']

        bpy.ops.object.select_all(action='DESELECT')

        if not objects:
            self.report({'WARNING'}, 'Nothing to do')
            return {'CANCELLED'}
            # active object MUST be 'MESH' type, otherwise an exception occurs (context is incorrect)
            # ask a question to somebody, why this happens???
        context.view_layer.objects.active = objects[0]

        for ob in objects:
            uv_layers_names = [uv.name for uv in ob.data.uv_layers]
            if props.desired_uv_map in uv_layers_names:
                ob.data.uv_layers[props.desired_uv_map].active = True
                ob.data.uv_layers[props.desired_uv_map].active_render = True

        return {'FINISHED'}


classes = [
    FT_OT_remove_mesh_without_something,
    FT_OT_fix_rotation,
    FT_OT_fix_scale,
    FT_OT_fix_x_axis_mirror,
    FT_OT_use_as_ground,
    FT_OT_set_desired_uv
    ]

class_register, class_unregister = bpy.utils.register_classes_factory(classes)
